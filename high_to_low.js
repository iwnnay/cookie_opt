class Inventory {
  static getUpgrade(index = 0) {
    const upgradeSelector = '#store .crate.upgrade.enabled';
    const upgrades = document.querySelectorAll(upgradeSelector);

    if (!upgrades.length) {
      return false;
    }

    if (index === -1) {
      return upgrades[upgrades.length - 1];
    }
    return upgrades[index];
  }

  static getProduct(index = 0) {
    const productSelector = '.product.unlocked.enabled';
    const products = document.querySelectorAll(productSelector);

    if (!products.length) {
      return false;
    }

    if (index === -1) {
      return products[products.length - 1];
    }
    return products[index];
  }
}

class Remote {
  static clickHighestUpgrade() {
    const upgrade = Inventory.getUpgrade(-1);
    if (upgrade) {
      upgrade.click();
      return true;
    }
    return false;
  }

  static clickLowestUpgrade() {
    const upgrade = Inventory.getUpgrade();
    if (upgrade) {
      upgrade.click();
      return true;
    }
    return false;
  }

  static clickHighestProduct() {
    const product = Inventory.getProduct(-1);
    if (product) {
      product.click();
      return true;
    }
    return false;
  }

  static clickLowestProduct() {
    const product = Inventory.getProduct();
    if (product) {
      product.click();
      return true;
    }
    return false;
  }
}

class CookieTimer {
  constructor() {
    this.clickInterval = false;
    this.clickTimeout = false;
  }

  clear() {
    if (this.clickInterval) {
      clearInterval(this.clickInterval);
    }

    if (this.clickTimeout) {
      clearTimeout(this.clickTimeout);
    }
  }

  setInterval(func, seconds) {
    this.clickInterval = setInterval(func, seconds * 1000);
  }

  setTimeout(func, seconds) {
    this.clickTimeout = setTimeout(func, seconds * 1000);
  }
}

class Stats {
  constructor() {
    this.interval = 5;
    this.minutes = 0;
    this.timer = false;
    this.data = [];
  }

  incrementMinutesWithInterval() {
    this.minutes += this.interval;
  }

  collectData() {
    this.data.push({ minutes: this.minutes, baked: this.baked });
  }

  delayedCollectData() {
    this.timer = setTimeout(() => {
      this.incrementMinutesWithInterval();
      this.collectData();
      this.delayedCollectData();
    }, this.interval * 60000);
  }

  start() {
    this.delayedCollectData();
  }

  get baked() {
    return Math.round(Game.cookiesEarned) || 0;
  }

  download(filename = 'cookie_data') {
    let csvContent = "data:text/csv;charset=utf-8," +
      this.data.map(e => [e.minutes, e.baked].join(',')).join('\n');

      const element = document.createElement('a');
      element.setAttribute('href', encodeURI(csvContent));
      element.setAttribute('download', `${filename}.csv`);
      element.style.display = 'none';

      document.body.appendChild(element);
      element.click();
      document.body.removeChild(element);
  }
}


class CookieOpt {
  constructor() {
    this.timer = new CookieTimer();
    this.stats = new Stats();
    this.randomMax = 300;
    this.value = 1;
    this.direction = 'high_to_low';
  }
  
  action() {
    if (Remote.clickHighestUpgrade() || Remote.clickHighestProduct()) {
      setTimeout(this.action.bind(this), 10);
    }
  }

  start(userValue) {
    this.value = this.generateValue(userValue);
    this.setupAndLaunchGame(() => {
      this.stats.start();
      this.setNextAction();
    });
  }
    
  setNextAction(){
    console.log({
      'Next click': this.whenValueTriggers(),
      value: this.value,
      direction: this.direction,
    });

    this.timer.clear();
    this.timer.setTimeout(() => {
      this.action();
      this.setNextAction();
    }, this.value);
  }

  generateValue(userValue) {
    if (userValue) {
      return userValue;
    }
    return Math.round(Math.random() * this.randomMax);
  }

  whenValueTriggers() {
    const dt = new Date();
    dt.setSeconds(dt.getSeconds() + this.value);
    return dt;
  }
  
  downloadStats() {
    this.stats.download(`${this.value}_${this.direction}`);
  }

  setupAndLaunchGame(callBack = ()=>{}) {
    const needToBake = 15 - this.stats.baked
    const done = () => {
      Remote.clickLowestProduct();
      callBack();
    }
    if (needToBake > 0) {
      for (let i = 1; i <= needToBake; i += 1) {
        setTimeout(() => {
          Game.ClickCookie();
        }, i * 10)

        if (i === needToBake) {
          setTimeout(done, i * 20);
        }
      }
    } else {
      done();
    }
  }
}

co = new CookieOpt();