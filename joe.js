class Clerk {
  static clickBigCookie() {
    Game.ClickCookie();
  }

  static click(name) {
    const item = this.get(name);

    if (this.cookies < item.getPrice()) {
      return false;
    }

    if (item.l) {
      item.l.click();
    } else {
      item.click();
    }

    return true;
  }

  static get(name) {
    return Game.Objects[name] || Game.UpgradesInStore.find(u => u.name === name) || false;
  }

  static isUpgradeInStock(name) {
    return Boolean(this.availableUpgrades.find(u => u.name === name));
  }

  static get products() {
    return Object.values(Game.Objects);
  }

  static get productNames() {
    return Object.keys(Game.Objects);
  }

  static get globalMultiplier() {
    return Game.globalCpsMult;
  }

  static get productCounts() {
    return this.products.reduce((counts, product) => {
      counts[product.name] = product.amount;
      return counts;
    }, {});
  }

  static get baked() {
    return Math.round(Game.cookiesEarned) || 0;
  }

  static get cookies() {
    return Math.floor(Game.cookies);
  }

  static get cookiesPerSecond() {
    return Game.cookiesPs;
  }

  static get availableUpgrades() {
    return Object.values(Game.UpgradesInStore);
  }

  static get boughtUpgrades() {
    return Object.values(Game.Upgrades).filter(u => u.bought).map(u => u.name);
  }

  static isGrandmaSynergy(name) {
    return Game.GrandmaSynergies.includes(name);
  }
}

class Calculator {
  static findTheNextBestThingToClick() {
    const calc = new Calculator();
    calc.syncCookieCounts();
    calc.weightUpgrades();
    calc.weightProducts();
    return calc.bigBaller;
  }

  constructor() {
    this.targets = [];
    this.bigBaller = null;
    this.bankedCookies = 0;
    this.cookiesPerSecond = 0;
  }

  syncCookieCounts() {
    this.bankedCookies = Clerk.cookies;
    this.cookiesPerSecond = Clerk.cookiesPerSecond;
  }

  calcDelayFromPriceAndOrGain(price, gain = 0) {
    return price < 0 ? 1 : price / (this.cookiesPerSecond + gain);
  }

  calculateCursorUpgradeGain(upgrade) {
    const rates = {
      'Thousand fingers': 0.1,
      'Million fingers': 0.5,
      'Billion fingers': 5,
      'Trillion fingers': 50,
      'Quadrillion fingers': 500,
      'Quintillion fingers': 5000,
      'Sextillion fingers': 50000,
      'Septillion fingers': 500000,
      'Octillion fingers': 5000000,
    };
    let bonus = rates[upgrade.name] || 0;
    if (bonus) {
      return bonus * Clerk.products.filter(o => o.name !== 'Cursor')
        .reduce((m, o) => m + o.amount, 0);
    }
    return Clerk.get('Cursor').storedTotalCps;
  }

  calculateKittenGain(upgrade) {
    const increases = {
      'Kitten helpers': 0.1,
      'Kitten workers': 0.125,
      'Kitten engineers': 0.15,
      'Kitten overseers': 0.175,
      'Kitten managers': 0.2,
      'Kitten accountants': 0.2,
      'Kitten specialists': 0.2,
      'Kitten experts': 0.2,
      'Kitten consultants': 0.2,
      'Kitten assistants to the regional manager': 0.175,
      'Kitten marketeers': 0.15,
      'Kitten analysts': 0.125,
      'Kitten executives': 0.115,
      'Kitten angels': 0.1,
      'Fortune #103': 0.05,
    };
    return (increases[upgrade.name]) * this.cookiesPerSecond;
  }

  calcUpgradeTarget(upgrade) {
    const price = upgrade.getPrice();
    let gain = 0;
    if (upgrade.kitten) { // kittens
      gain = this.calculateKittenGain(upgrade);

    } else if (upgrade.pool === 'cookie') { // Cookies
      const power = typeof upgrade.power === 'function' ? upgrade.power() : upgrade.power;
      gain = this.cookiesPerSecond * (power * 0.01);

    } else if (Clerk.isGrandmaSynergy(upgrade.name)) { // Grandma Synergy
      const { amount, storedTotalCps } = Clerk.get('Grandma');
      gain = storedTotalCps +
        Math.floor(amount / (upgrade.buildingTie.id - 1)) * upgrade.buildingTie.storedTotalCps * 0.01;

    } else if (upgrade.buildingTie2) { // Regular Synergy
      const b1 =  upgrade.buildingTie1;
      const b2 = upgrade.buildingTie2;
      gain = (b1.storedTotalCps * .05 * b2.amount) +
        (b2.storedTotalCps * .001 * b1.amount);

    } else if (!upgrade.buildingTie1) { // Cursors
      gain = this.calculateCursorUpgradeGain(upgrade);

    } else { // Default
      gain = upgrade.buildingTie1.storedTotalCps;
    }

    return {
      gain,
      price,
      wait: this.calcDelayFromPriceAndOrGain(price - this.bankedCookies),
      name: upgrade.name,
      type: 'upgrade',
    };
  }

  calcProductTarget(product) {
    const price = product.getPrice();
    return {
      gain: product.storedCps,
      price,
      wait: this.calcDelayFromPriceAndOrGain(price - this.bankedCookies),
      name: product.name,
      type: 'product',
    };
  }

  finalTimeFromPurchaseOrder(first, second) {
    return first.wait + this.calcDelayFromPriceAndOrGain(second.price, first.gain);
  }

  targetBallsBigger(target) {
    return this.finalTimeFromPurchaseOrder(target, this.bigBaller) <
      this.finalTimeFromPurchaseOrder(this.bigBaller, target);
  }

  setBigBaller(target) {
    if (!this.bigBaller || this.targetBallsBigger(target)) {
      this.bigBaller = target;
    }
  }

  weightUpgrades() {
    Clerk.availableUpgrades.forEach((o) => {
      // Click upgrades cick in at 1,000 clicks, would need to be added
      const target = this.calcUpgradeTarget(o);
      this.setBigBaller(target);
      this.targets.push(target);
    });
  }


  weightProducts() {
    Clerk.products.forEach((o) => {
      const target = this.calcProductTarget(o);
      this.setBigBaller(target);
      this.targets.push(target);
    });
  }
}

class DelayedClick {
  constructor() {
    this.target = null;
    this.delay = 0;
  }

  afterClick(callBack) {
    if (callBack) {
      this.afterClickCallBack = callBack;
      return;
    }

    this.afterClickCallBack();
  }

  lockNextTarget() {
    const startCPS = Clerk.cookiesPerSecond;
    this.target = Calculator.findTheNextBestThingToClick();
    if (this.target.clickNow) {
      this.delay = 1;
    } else {
      this.delay =  this.target.wait * 1000;
    }

    new Promise((resolve) => {
      let clicked = false;
      const { type } = this.target;
      const clickTargetElement = (resolve) => {
        if (clicked || Clerk.click(this.target.name)) {
          clicked = true;
          if (
              (type === 'product' && startCPS < Clerk.cookiesPerSecond) ||
              (type === 'upgrade' && !Clerk.isUpgradeInStock(this.target.name))
          ) {
            return resolve();
          }
        }
        setTimeout(() => { clickTargetElement(resolve); }, 10);
      };

      setTimeout(() => { clickTargetElement(resolve); }, this.delay);
    })
    .then(() => this.afterClick())
    .catch((e) => { throw(e); });
  }

  get delayInSeconds() {
    return this.delay === 1 ? 0 : Math.round(this.delay / 1000);
  }

  get nextClickTime() {
    const dt = new Date();
    dt.setSeconds(dt.getSeconds() + this.delayInSeconds);
    return `${dt.getHours()}:${dt.getMinutes()}:${dt.getSeconds()}`;
  }
}

class Stats {
  constructor() {
    this.startDateTime = new Date();
    this.previousEventTime = new Date();
    this.interval = 5;
    this.minutes = 0;
    this.timer = false;
    this.data = [];
    this.events = [];
  }

  incrementMinutesWithInterval() {
    this.minutes += this.interval;
  }

  collectData() {
    const datum = { minutes: this.minutes, baked: Clerk.baked, ...Clerk.productCounts };
    this.data.push(datum);
  }

  delayedCollectData() {
    this.timer = setTimeout(() => {
      this.incrementMinutesWithInterval();
      this.collectData();
      this.delayedCollectData();
    }, this.interval * 60000);
  }

  start() {
    this.delayedCollectData();
  }

  get timeFromStart() {
    return this.humanReadableTimeDifference(this.startDateTime, new Date());
  }

  get timeFromPreviousEveent() {
    return this.humanReadableTimeDifference(this.previousEventTime, new Date());
  }

  humanReadableTimeDifference(date1, date2) {
    let difference = date2.getTime() - date1.getTime();

    const miliDays = 1000 * 24 * 360;
    const miliHours = 1000 * 360;
    const miliMinutes = 1000 * 60;
    const miliSeconds = 1000;

    const days = Math.floor(difference / miliDays);
    difference -= days * miliDays;

    const hours = Math.floor(difference / miliHours);
    difference -= hours * miliHours;

    const minutes = Math.floor(difference / miliMinutes);
    difference -= minutes * miliMinutes;

    const seconds = Math.floor(difference / miliSeconds);

    return [days, hours, minutes, seconds].join(':');
  }

  get timerData() {
    return {
      timeElapsed: this.timeFromStart,
      sinceLastEvent: this.timeFromPreviousEvent,
    };
  }

  get gameData() {
    return {
      baked: Clerk.baked,
      banked: Clerk.cookies,
      amounts: Clerk.productAmounts,
      upgrades: Clerk.boughtUpgrades,
    };
  }

  addEvent(eventName, opt) {
    const { clicker } = opt;
    const { target } = clicker;
    const create = {
      'setNextAction': () => {
        return {
          ...this.timerData,
          game: this.gameData,
          delay: clicker.delayInSeconds,
          ...target,
        };
      }
    };
    this.events.push(create[eventName]());
    if (clicker) {
      console.log({
        'Next click': clicker.nextClickTime,
        'Waiting to click': target.name,
        delay: clicker.delayInSeconds,
        run: opt.filename,
        minutesRun: this.minutes,
      });
    }
    this.previousEventTime = new Date();
  }

  downloadCSV(filename = 'cookie_data') {
    const columns = ['minutes', 'baked', ...Clerk.productNames];
    const table = this.data.map((e) => columns.map(c => e[c]).join(','))
    let content = "data:text/csv;charset=utf-8,";

    table.unshift(columns.join(','));
    content += table.join('\n');

    this.downloadFile(`${filename}.csv`, content);
  }

  downloadJSON(filename = 'cookie_data') {
    console.log('events', this.events);
    const content = "data:application/json;charset=utf-8," +
      JSON.stringify({ events: this.events });
    this.downloadFile(`${filename}.json`, content);
  }

  downloadFile(filename, content) {
    const element = document.createElement('a');
    element.setAttribute('href', encodeURI(content));
    element.setAttribute('download', filename);
    element.style.display = 'none';

    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }
}


class CookieOpt {
  constructor() {
    this.clicker = new DelayedClick();
    this.stats = new Stats();
    this.filename = 'joev8_old_lady_synergy_not_cursor';
  }

  start() {
    this.setupAndLaunchGame(() => {
      this.stats.start();
      this.clicker.afterClick(this.setNextAction.bind(this));
      this.setNextAction();
    });
  }

  setNextAction(){
    this.clicker.lockNextTarget();
    this.stats.addEvent('setNextAction', this);
  }

  downloadStats() {
    this.stats.downloadCSV(`${this.filename}_${this.stats.minutes}`);
  }

  downloadEvents() {
    this.stats.downloadJSON(`${this.filename}_${this.stats.minutes}`);
  }

  setupAndLaunchGame(callBack = ()=>{}) {
    const clickToFifteen = () => {
      if (15 - Clerk.baked > 0) {
        Clerk.clickBigCookie();
        setTimeout(clickToFifteen, 10);
      } else if (Clerk.cookiesPerSecond > 0) {
        callBack();
      } else {
        Clerk.click('Cursor');
        setTimeout(clickToFifteen, 10);
      }
    }
    clickToFifteen();
  }
}

const co = new CookieOpt();
co.start();
